#include "ADC.h"
#include <IntervalTimer.h>
#include <SD.h>
#include <SPI.h>
#include <Wire.h>
#include <TimeLib.h>

const int chipSelect = BUILTIN_SDCARD;
IntervalTimer myTimer;
ADC *adc = new ADC(); // adc object
File myFile;

volatile bool DEBUGON = true;

int vin[10];

int out[2];

int sout = 0;   ///safe finakl dac output value

const int mdac1 = 2;

const int sdel = 10;  //turn this up until your plotter or terminal stops freaking out


const int sampfreq = 5500; //in HZ
 
void setup()
{
  Wire.begin();
  // put your setup code here, to run once:
pinMode(13, OUTPUT); //debug led

setTime(16, 33, 0, 2, 9, 2022); // set time(hr, min, sec, day, month, year)

pinMode(mdac1, OUTPUT); //DAC OUT


myTimer.begin(sampledat, 1000000/sampfreq);  //set up interval timer, name it's funtion

Serial.begin(2000000);  //set up terminal out
Serial1.begin(115200);
Serial2.begin(115200);

    adc->adc0->setAveraging(0); // set number of averages
    adc->adc0->setResolution(16); // set bits of resolution
    adc->adc0->setConversionSpeed(ADC_CONVERSION_SPEED::VERY_HIGH_SPEED); // change the conversion speed
    adc->adc0->setSamplingSpeed(ADC_SAMPLING_SPEED::HIGH_SPEED); // change the sampling speed

    analogWriteFrequency(mdac1, 468750 );  //set pwm out 2 to this magic number i found
    analogWriteResolution(16);  //set resolution to max


  Serial.print("Initializing SD card...");

  if (!SD.begin(chipSelect)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");
  
  // open the file. 
  myFile = SD.open("testFinal.txt", FILE_WRITE);
  
  // if the file opened okay, write to it:
  if (myFile) {
    Serial.print("Writing to test.txt PIIID...");
    myFile.print("testing 1, 2, 3. ");
    myFile.print(String(day()));
    myFile.print("/");
    myFile.print(String(month()));
    myFile.print("/");
    myFile.print(String(year()));
    myFile.print("\n");
    myFile.close();
    Serial.println("done.");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
  
  // re-open the file for reading:
  myFile = SD.open("testFinal.txt");
  if (myFile) {
    Serial.println("test.txt:");
    
    // read from the file until there's nothing else in it:
    while (myFile.available()) {
      Serial.write(myFile.read());
    }
    // close the file:
    myFile.close();
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
  delay(1000);


}



uint64_t scount;

void sampledat()  ///called at pamplefreq no matter what, unless crashed.
{
  digitalWriteFast(13, 1);   //turn on debug LED
 
  for(uint8_t i = 0; i < 8; i++)
  {
    vin[i] = double(adc->adc0->analogRead(i)) - 32767;  //make zero centered, store             //filter fun
  }

  scount ++;  //index sample counter

  digitalWriteFast(13, 0);  //turn off debug LED

 
  //vin[0]  == "HDR1 X1    DC"
  //vin[1]  == "HDR1 X10   HPF 3Hz"
  //vin[2]  == "HDR1 X100  HPF 31Hz"
  //vin[3]  == "HDR1 X1000 HPF 314Hz"
  //vin[4]  == "HDR2 X1    DC"
  //vin[5]  == "HDR2 X10   HPF 3Hz"
  //vin[6]  == "HDR2 X100  HPF 31Hz"
  //vin[7]  == "HDR2 X1000 HPF 314Hz"


  ////


  ////your dsp programming here


  ///
 
 
  sout = vin[0] + 32767;   //make zero cntered data into 32768-centred
  analogWrite(mdac1, sout);   //push safe value to DAC1 from 0 to 65535

 
 

 

}

float valD = 0.0;

void loop() //barf data at terminal as fast as possible
{

  if (DEBUGON)
  {
  Serial.print(32768);
  delayMicroseconds(sdel);
  Serial.print("\t");
  delayMicroseconds(sdel);
  Serial.print(-32768);
  delayMicroseconds(sdel);
  Serial.print("\t");
  delayMicroseconds(sdel);
  
  for(uint8_t i = 0; i < 4; i++)
  {
   
    Serial.print(int(vin[i]));
    delayMicroseconds(sdel);
    Serial.print("\t");
    delayMicroseconds(sdel);
   
  }
  if(Serial1.available()) {
    byte rlByte = Serial1.read();
    int byteNow = rlByte * 1;
    valD = (float)byteNow / 17.0;
  }
  Serial.print(valD);
  myFile = SD.open("testFinal.txt", FILE_WRITE);
  
  // if the file opened okay, write to it:
  if (myFile) {
    for(uint8_t i = 0; i < 4; i++) {
      myFile.print(int(vin[i]));
      myFile.print("\t");
    }  // close the file:
    myFile.print(valD);
    myFile.print("\t");
    myFile.print(hour());
    myFile.print(":");
    myFile.print(minute());
    myFile.print(":");
    myFile.print(second());
    myFile.print("\n");
  } else {
    // if the file didn't open, print an error:
  }
  myFile.close();
  
  Serial.print("\n");
  }

}
